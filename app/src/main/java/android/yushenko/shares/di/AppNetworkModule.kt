package android.yushenko.shares.di

import android.yushenko.shares.data.api.ApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppNetworkModule {

    private val BASE_URL_SHARES = "https://www.alphavantage.co/"

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

    @Provides
    fun provideSharesRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL_SHARES)
            .client(provideHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    fun provideSharesService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)



}