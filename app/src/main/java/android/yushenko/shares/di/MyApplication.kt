package android.yushenko.shares.di

import android.app.Application

class MyApplication : Application() {


    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appDatabaseModule(AppDatabaseModule(this))
            .build()
    }

    fun getAppComponent(): AppComponent = appComponent
}