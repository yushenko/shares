package android.yushenko.shares.di

import android.yushenko.shares.ui.main.MainFragment
import android.yushenko.shares.ui.search.SearchFragment
import android.yushenko.shares.ui.tracked.TrackedFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppNetworkModule::class, AppDatabaseModule::class])
interface AppComponent {

    fun inject(searchFragment: SearchFragment)
    fun inject(mainFragment: MainFragment)
    fun inject(trackedFragment: TrackedFragment)
}