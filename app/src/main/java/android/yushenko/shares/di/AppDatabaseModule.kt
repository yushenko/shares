package android.yushenko.shares.di

import android.app.Application
import android.content.Context
import android.yushenko.shares.data.database.AppDatabase
import android.yushenko.shares.data.database.TrackedDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppDatabaseModule(private val application: Application) {

    @Singleton
    @Provides
    fun provideRoomDbInstance(): AppDatabase =
        AppDatabase.getInstance(provideAppContext())

    @Singleton
    @Provides
    fun provideAppContext(): Context = application.applicationContext


    @Singleton
    @Provides
    fun provideDao(db: AppDatabase): TrackedDao = db.trackedDao()

}