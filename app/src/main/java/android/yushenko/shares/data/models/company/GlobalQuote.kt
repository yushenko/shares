package android.yushenko.shares.data.models.company

import com.google.gson.annotations.SerializedName

data class GlobalQuote (
    @SerializedName("01. symbol")
    var symbol: String,

    @SerializedName("02. open")
    var open: Double,

    @SerializedName("03. high")
    var high: Double,

    @SerializedName("04. low")
    var low: Double,

    @SerializedName("05. price")
    var price: Double,

    @SerializedName("06. volume")
    var volume: String,

    @SerializedName("07. latest trading day")
    var latestTradingDay: String,

    @SerializedName("08. previous close")
    var previousClose: String,

    @SerializedName("09. change")
    var change: Double,

    @SerializedName("10. change percent")
    var changePercent: String
)