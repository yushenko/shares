package android.yushenko.shares.data.repository

import android.yushenko.shares.data.database.Tracked
import android.yushenko.shares.data.database.TrackedDao
import android.yushenko.shares.data.models.search.Search
import java.util.*
import javax.inject.Inject

class LocalDataSource @Inject constructor(
    private val dao: TrackedDao
) {
    fun setTracked(search: Search) {
        val tracked =
            Tracked(UUID.randomUUID().toString(), search.symbol, search.name, search.region)
        val findTrack = tracked.symbol?.let { dao.findByTrack(it) }
        if (findTrack?.symbol == tracked.symbol) findTrack?.let { dao.delete(it) }
        dao.insertAll(tracked)
    }

    fun getAll() = dao.getAll()

    fun deleteItem(symbol: String) = dao.delete(dao.findByTrack(symbol))
}