package android.yushenko.shares.data.models.search


import com.google.gson.annotations.SerializedName

data class Search(

    @SerializedName("1. symbol")
    val symbol: String? = null,

    @SerializedName("2. name")
    val name: String? = null,

    @SerializedName("4. region")
    val region: String? = null,

    @SerializedName("8. currency")
    val currency: String? = null,

    @SerializedName("9. matchScore")
    val matchScore: String? = null
)