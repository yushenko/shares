package android.yushenko.shares.data.models.graph


import android.graphics.Point
import com.google.gson.annotations.SerializedName

data class Result(
    var targetPos_x: Float,
    var targetPos_y: Float,

    @SerializedName("c")
    val c: Double,
    @SerializedName("h")
    val h: Double,
    @SerializedName("l")
    val l: Double,
    @SerializedName("n")
    val n: Int,
    @SerializedName("o")
    val o: Double,
    @SerializedName("t")
    val t: Long,
    @SerializedName("v")
    val v: Double,
    @SerializedName("vw")
    val vw: Double
)