package android.yushenko.shares.data.models.news


import com.google.gson.annotations.SerializedName

data class NewsItem(
    @SerializedName("image")
    val image: String,
    @SerializedName("keywords")
    val keywords: List<String>,
    @SerializedName("source")
    val source: String,
    @SerializedName("summary")
    val summary: String,
    @SerializedName("symbols")
    val symbols: List<String>,
    @SerializedName("timestamp")
    val timestamp: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("url")
    val url: String
)