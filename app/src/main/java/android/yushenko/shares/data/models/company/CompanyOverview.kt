package android.yushenko.shares.data.models.company

import com.google.gson.annotations.SerializedName

data class CompanyOverview (
    
    @SerializedName("Symbol")
    var symbol: String? = null,

    @SerializedName("AssetType")
    var assetType: String? = null,

    @SerializedName("Name")
    var name: String? = null,

    @SerializedName("Description") 
    var description: String? = null,

    @SerializedName("CIK")
    var cik: String? = null,

    @SerializedName("Exchange")
    var exchange: String? = null,

    @SerializedName("Currency")
    var currency: String? = null,

    @SerializedName("Country")
    var country: String? = null,

    @SerializedName("Sector")
    var sector: String? = null,

    @SerializedName("Industry")
    var industry: String? = null,

    @SerializedName("Address")
    var address: String? = null,

    @SerializedName("FullTimeEmployees")
    var fullTimeEmployees: String? = null,

    @SerializedName("FiscalYearEnd")
    var fiscalYearEnd: String? = null,

    @SerializedName("LatestQuarter")
    var latestQuarter: String? = null,

    @SerializedName("MarketCapitalization")
    var marketCapitalization: String? = null,

    @SerializedName("EBITDA")
    var ebitda: String? = null,

    @SerializedName("PERatio")
    var pERatio: String? = null,

    @SerializedName("PEGRatio")
    var pEGRatio: String? = null,

    @SerializedName("BookValue")
    var bookValue: String? = null,

    @SerializedName("DividendPerShare")
    var dividendPerShare: String? = null,

    @SerializedName("DividendYield")
    var dividendYield: String? = null,

    @SerializedName("EPS")
    var eps: String? = null,

    @SerializedName("RevenuePerShareTTM")
    var revenuePerShareTTM: String? = null,

    @SerializedName("ProfitMargin")
    var profitMargin: String? = null,

    @SerializedName("OperatingMarginTTM")
    var operatingMarginTTM: String? = null,

    @SerializedName("ReturnOnAssetsTTM")
    var returnOnAssetsTTM: String? = null,

    @SerializedName("ReturnOnEquityTTM")
    var returnOnEquityTTM: String? = null,

    @SerializedName("RevenueTTM")
    var revenueTTM: String? = null,

    @SerializedName("GrossProfitTTM")
    var grossProfitTTM: String? = null,

    @SerializedName("DilutedEPSTTM")
    var dilutedEPSTTM: String? = null,

    @SerializedName("QuarterlyEarningsGrowthYOY")
    var quarterlyEarningsGrowthYOY: String? = null,

    @SerializedName("QuarterlyRevenueGrowthYOY")
    var quarterlyRevenueGrowthYOY: String? = null,

    @SerializedName("AnalystTargetPrice")
    var analystTargetPrice: String? = null,

    @SerializedName("TrailingPE")
    var trailingPE: String? = null,

    @SerializedName("ForwardPE")
    var forwardPE: String? = null,

    @SerializedName("PriceToSalesRatioTTM")
    var priceToSalesRatioTTM: String? = null,

    @SerializedName("PriceToBookRatio")
    var priceToBookRatio: String? = null,

    @SerializedName("EVToRevenue")
    var eVToRevenue: String? = null,

    @SerializedName("EVToEBITDA")
    var eVToEBITDA: String? = null,

    @SerializedName("Beta")
    var beta: String? = null,

    @SerializedName("52WeekHigh")
    private var _52WeekHigh: String? = null,

    @SerializedName("52WeekLow")
    private var _52WeekLow: String? = null,

    @SerializedName("50DayMovingAverage")
    private var _50DayMovingAverage: String? = null,

    @SerializedName("200DayMovingAverage")
    private var _200DayMovingAverage: String? = null,

    @SerializedName("SharesOutstanding")
    var sharesOutstanding: String? = null,

    @SerializedName("SharesFloat")
    var sharesFloat: String? = null,

    @SerializedName("SharesShort")
    var sharesShort: String? = null,

    @SerializedName("SharesShortPriorMonth")
    var sharesShortPriorMonth: String? = null,

    @SerializedName("ShortRatio")
    var shortRatio: String? = null,

    @SerializedName("ShortPercentOutstanding")
    var shortPercentOutstanding: String? = null,

    @SerializedName("ShortPercentFloat")
    var shortPercentFloat: String? = null,

    @SerializedName("PercentInsiders")
    var percentInsiders: String? = null,

    @SerializedName("PercentInstitutions")
    var percentInstitutions: String? = null,

    @SerializedName("ForwardAnnualDividendRate")
    var forwardAnnualDividendRate: String? = null,

    @SerializedName("ForwardAnnualDividendYield")
    var forwardAnnualDividendYield: String? = null,

    @SerializedName("PayoutRatio")
    var payoutRatio: String? = null,

    @SerializedName("DividendDate")
    var dividendDate: String? = null,

    @SerializedName("ExDividendDate")
    var exDividendDate: String? = null,

    @SerializedName("LastSplitFactor")
    var lastSplitFactor: String? = null,

    @SerializedName("LastSplitDate")
    var lastSplitDate: String? = null
)