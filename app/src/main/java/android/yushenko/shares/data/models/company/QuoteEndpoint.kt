package android.yushenko.shares.data.models.company

import com.google.gson.annotations.SerializedName

data class QuoteEndpoint(

    @SerializedName("Global Quote")
    val globalQuote: GlobalQuote
)