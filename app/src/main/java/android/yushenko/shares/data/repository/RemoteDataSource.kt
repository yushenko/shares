package android.yushenko.shares.data.repository

import android.yushenko.shares.data.api.ApiService
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val api: ApiService
) {

    suspend fun getSearch(keywords: String) = api.getSearch(keywords)

    suspend fun getShares(symbol: String) = api.getShares(symbol)

    suspend fun getNews(symbol: String) = api.getNews(symbol)

    suspend fun getDataDays(symbol: String) = api.getDataDays(symbol, buildDatePatch())

    private fun buildDatePatch(): String {
        val dateEnd = SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Date())
        val calendar = GregorianCalendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -30)
        val date = Date((calendar.timeInMillis/1000) * 1000L)
        val dateStart = SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date)
        return "$dateStart/$dateEnd"
    }
}