package android.yushenko.shares.data.api


import android.yushenko.shares.data.models.company.CompanyOverview
import android.yushenko.shares.data.models.company.QuoteEndpoint
import android.yushenko.shares.data.models.graph.DataDays
import android.yushenko.shares.data.models.news.News
import android.yushenko.shares.data.models.search.SearchEndpoint
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    //    https://www.alphavantage.co/documentation/ (data shares)

    @GET("query?function=SYMBOL_SEARCH&apikey=7bbc3c5045msh25002ef9f56fb8ep1518f2jsn7b837503abcd")
    suspend fun getSearch(@Query("keywords") keywords: String?): SearchEndpoint

    @GET("query?function=GLOBAL_QUOTE&apikey=7bbc3c5045msh25002ef9f56fb8ep1518f2jsn7b837503abcd")
    suspend fun getShares(@Query("symbol") symbol: String?): QuoteEndpoint

    @GET("query?function=OVERVIEW&apikey=7bbc3c5045msh25002ef9f56fb8ep1518f2jsn7b837503abcd")
    suspend fun getCompanyOverview(@Query("symbol") symbol: String?): CompanyOverview


    //    https://polygon.io/docs/get_v2_reference_news_anchor (news)

    @GET("https://api.polygon.io/v1/meta/symbols/{symbol}/news?perpage=10&page=1&apiKey=CdLI8RIUXE9R4kWscGZPZzhp3n4wOocs")
    suspend fun getNews(@Path("symbol") symbol: String?): News

    @GET("https://api.polygon.io/v2/aggs/ticker/{symbol}/range/1/day/{date_patch}?unadjusted=true&sort=asc&limit=120&apiKey=CdLI8RIUXE9R4kWscGZPZzhp3n4wOocs")
    suspend fun getDataDays(@Path("symbol") symbol: String?, @Path("date_patch") datePatch: String?): DataDays
}