package android.yushenko.shares.data.models.graph


import com.google.gson.annotations.SerializedName

data class DataDays(
    @SerializedName("adjusted")
    val adjusted: Boolean,
    @SerializedName("count")
    val count: Int,
    @SerializedName("queryCount")
    val queryCount: Int,
    @SerializedName("request_id")
    val requestId: String,
    @SerializedName("results")
    val results: List<Result>,
    @SerializedName("resultsCount")
    val resultsCount: Int,
    @SerializedName("status")
    val status: String,
    @SerializedName("ticker")
    val ticker: String
)