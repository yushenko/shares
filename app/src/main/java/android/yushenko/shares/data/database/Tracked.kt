package android.yushenko.shares.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Tracked(

    @PrimaryKey
    val uid: String,

    @ColumnInfo(name = "symbol")
    val symbol: String?,

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "region")
    val region: String?,
)