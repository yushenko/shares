package android.yushenko.shares.data.repository

import android.yushenko.shares.data.database.Tracked
import android.yushenko.shares.data.models.search.Search
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharesRepository @Inject constructor(
    private val remoteDataSourceSource: RemoteDataSource,
    private val localDataSourceSource: LocalDataSource
) {

    suspend fun getSearch(keywords: String) = remoteDataSourceSource.getSearch(keywords)

    fun addItemFollow(search: Search) = localDataSourceSource.setTracked(search)

    fun deleteItem(symbol: String) =localDataSourceSource.deleteItem(symbol)

    fun trackedAll() = localDataSourceSource.getAll()

    suspend fun getDataShares(symbol: String) = remoteDataSourceSource.getShares(symbol)

    suspend fun getNewsCompany(symbol: String) = remoteDataSourceSource.getNews(symbol)

    suspend fun getDataDays(symbol: String) = remoteDataSourceSource.getDataDays(symbol)
}