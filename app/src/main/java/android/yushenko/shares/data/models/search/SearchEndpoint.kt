package android.yushenko.shares.data.models.search

import com.google.gson.annotations.SerializedName

data class SearchEndpoint(

    @SerializedName("bestMatches")
    val searches: List<Search>
)