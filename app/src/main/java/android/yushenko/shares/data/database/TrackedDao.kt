package android.yushenko.shares.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TrackedDao {

    @Query("SELECT * FROM tracked")
    fun getAll(): LiveData<List<Tracked>>

    @Query("SELECT * FROM tracked WHERE symbol LIKE :symbol LIMIT 1")
    fun findByTrack(symbol: String): Tracked

    @Insert
    fun insertAll(users: Tracked)

    @Delete
    fun delete(tracked: Tracked)
}