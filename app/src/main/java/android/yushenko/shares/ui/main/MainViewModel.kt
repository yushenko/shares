package android.yushenko.shares.ui.main

import android.yushenko.shares.data.repository.SharesRepository
import android.yushenko.shares.ui.shared.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: SharesRepository
) : BaseViewModel() {

    fun trackedAll() = repository.trackedAll()
}