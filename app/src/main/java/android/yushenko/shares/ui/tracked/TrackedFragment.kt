package android.yushenko.shares.ui.tracked

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Display
import android.view.View
import android.yushenko.shares.R
import android.yushenko.shares.data.models.company.GlobalQuote
import android.yushenko.shares.di.MyApplication
import android.yushenko.shares.ui.activity.MainActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.idapgroup.lifecycle.ktx.observe
import kotlinx.android.synthetic.main.search_fragment.*
import kotlinx.android.synthetic.main.tracked_fragment.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.math.round

class TrackedFragment : Fragment(R.layout.tracked_fragment) {

    @Inject
    lateinit var viewModel: TrackedViewModel

    private val adapter = NewsAdapter {
        val address = Uri.parse(it.url)
        val intent = Intent(Intent.ACTION_VIEW, address)
        startActivity(intent)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (view.context.applicationContext as MyApplication).getAppComponent().inject(this)
        setupObserving()
        val args: TrackedFragmentArgs by navArgs()
        viewModel.loadSharesData(args.symbol)

        recyclerNews.layoutManager = LinearLayoutManager(requireActivity())

        val display: Display = requireActivity().windowManager.defaultDisplay

        recyclerNews.minimumHeight = display.height
        recyclerNews.adapter = adapter

        recyclerNews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                scrollView.scrollBy(0, dy)
            }
        })

        backClick.setOnClickListener {
            (activity as MainActivity).nawController.popBackStack()
        }

        deleteClick.setOnClickListener {
            viewModel.unFollow(args.symbol)
            (activity as MainActivity).nawController.popBackStack()
        }

    }

    private fun setupObserving() {
        observe(viewModel.sharesLiveData, {
            showSharesData(it.globalQuote)
        })

        observe(viewModel.dataDaysLiveData, {
            graphView.setData(it.results)
        })

        observe(viewModel.newsLiveData, {
            adapter.setData(it)
        })
    }

    private fun setColorGraph(globalQuote: GlobalQuote) {
        if (globalQuote.change >= 0.0)
            graphView.setGraphLineColor(Color.GREEN)
        else graphView.setGraphLineColor(Color.RED)
    }

    private fun showSharesData(globalQuote: GlobalQuote) {
        setColorGraph(globalQuote)

        textSymbol.text = globalQuote.symbol
        textDate.text = SimpleDateFormat("EEEE HH:mm", Locale.US).format(Date())

        textUsd.text = "$"
        textPrice.text = String.format("%.2f", globalQuote.price)
        textChange.text = "$${String.format("%.2f", globalQuote.change)}"

        val num = globalQuote.changePercent.replace("%", "")
        textChangePercent.text = "(${round(num.toFloat() * 100) / 100}%)"

        textOpen.text = resources.getText(R.string.open)
        textHigh.text = resources.getText(R.string.high)
        textLow.text = resources.getText(R.string.low)

        textOpenPrice.text = String.format("$%.2f", globalQuote.open)
        textHighPrice.text = String.format("$%.2f", globalQuote.high)
        textLowPrice.text = String.format("$%.2f", globalQuote.low)
    }

}