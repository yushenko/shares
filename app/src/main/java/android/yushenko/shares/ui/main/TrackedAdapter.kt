package android.yushenko.shares.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.yushenko.shares.R
import android.yushenko.shares.data.database.Tracked
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_tracked.view.*


class TrackedAdapter(
    private val onItemClicked: (Tracked) -> Unit
) : RecyclerView.Adapter<TrackedAdapter.TrackedHolder>() {

    private val listTracked: MutableList<Tracked> = mutableListOf()

    fun setData(list: List<Tracked>) {
        listTracked.clear()
        listTracked.addAll(list)
        notifyDataSetChanged()
    }

    class TrackedHolder(
        itemView: View,
        onItemClicked: (Int) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                onItemClicked(adapterPosition)
            }
        }

        fun bind(tracked: Tracked) {
            itemView.textSymbol.text = tracked.symbol
            itemView.textName.text = tracked.name
            itemView.textRegion.text = tracked.region
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackedHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_tracked, parent, false)
        return TrackedHolder(view) {
            onItemClicked(listTracked[it])
        }
    }

    override fun onBindViewHolder(holder: TrackedHolder, position: Int) {
        holder.bind(listTracked[position])
    }

    override fun getItemCount(): Int = listTracked.size

}