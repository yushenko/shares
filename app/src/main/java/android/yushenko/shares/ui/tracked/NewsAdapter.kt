package android.yushenko.shares.ui.tracked

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.yushenko.shares.R
import android.yushenko.shares.data.database.Tracked
import android.yushenko.shares.data.models.news.NewsItem
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_news.view.*
import kotlinx.android.synthetic.main.item_tracked.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class NewsAdapter(
    private val onItemClicked: (NewsItem) -> Unit
) : RecyclerView.Adapter<NewsAdapter.NewsHolder>() {

    private val listNews: MutableList<NewsItem> = mutableListOf()

    fun setData(list: ArrayList<NewsItem>) {
        listNews.clear()
        listNews.addAll(list)
        notifyDataSetChanged()
    }

    class NewsHolder(
        itemView: View,
        onItemClicked: (Int) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                onItemClicked(adapterPosition)
            }
        }

        fun bind(news: NewsItem) {
            Picasso.with(itemView.context)
            .load(news.image)
            .into(itemView.iconNews)
            itemView.textTitle.text = news.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
        return NewsHolder(view) {
            onItemClicked(listNews[it])
        }
    }

    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        holder.bind(listNews[position])
    }

    override fun getItemCount(): Int = listNews.size

}