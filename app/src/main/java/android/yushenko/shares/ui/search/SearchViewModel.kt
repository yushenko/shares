package android.yushenko.shares.ui.search


import android.yushenko.shares.data.models.search.Search
import android.yushenko.shares.data.models.search.SearchEndpoint
import android.yushenko.shares.data.repository.SharesRepository
import android.yushenko.shares.ui.shared.BaseViewModel
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val repository: SharesRepository
) : BaseViewModel() {

    val liveSearchEndpointData = MutableLiveData<SearchEndpoint>()

    fun getSearch(keywords: String) = launch {
        liveSearchEndpointData.postValue(repository.getSearch(keywords))
    }


    fun addItemFollow(search: Search) = launch {
        repository.addItemFollow(search)
    }
}