package android.yushenko.shares.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.yushenko.shares.R
import android.yushenko.shares.data.models.search.Search
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_search.view.*


class SearchAdapter(
    private val onItemClicked: (Search) -> Unit
) : RecyclerView.Adapter<SearchAdapter.SearchHolder>() {

    private val listSearches: MutableList<Search> = mutableListOf()

    fun setData(list: List<Search>) {
        listSearches.clear()
        listSearches.addAll(list)
        notifyDataSetChanged()
    }

    class SearchHolder(
        itemView: View,
        onItemClicked: (Int) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.btnFollow.setOnClickListener {
                onItemClicked(adapterPosition)
            }
        }

        fun bind(search: Search) {
            itemView.textSymbol.text = search.symbol
            itemView.textName.text = search.name
            itemView.textRegion.text = search.region
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search, parent, false)
        return SearchHolder(view) {
            onItemClicked(listSearches[it])
        }
    }

    override fun onBindViewHolder(holder: SearchHolder, position: Int) {
        holder.bind(listSearches[position])
    }

    override fun getItemCount(): Int = listSearches.size

}