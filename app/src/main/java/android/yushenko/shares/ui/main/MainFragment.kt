package android.yushenko.shares.ui.main

import android.os.Bundle
import android.view.View
import android.yushenko.shares.ui.activity.MainActivity
import android.yushenko.shares.R
import android.yushenko.shares.di.MyApplication
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.idapgroup.lifecycle.ktx.observe
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject

class MainFragment : Fragment(R.layout.main_fragment) {

    @Inject
    lateinit var viewModel: MainViewModel

    private val adapter = TrackedAdapter {
        val action = MainFragmentDirections.actionMainFragmentToTrackedFragment(it.symbol!!)
        findNavController().navigate(action)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (view.context.applicationContext as MyApplication).getAppComponent().inject(this)
        setupObserver()

        recyclerTracked.layoutManager = LinearLayoutManager(requireActivity())
        recyclerTracked.adapter = adapter



        searchClick.setOnClickListener {
            (activity as MainActivity).nawController.navigate(R.id.action_mainFragment_to_searchFragment)
        }

    }

    private fun setupObserver() {
        observe(viewModel.trackedAll(), {
            adapter.setData(it.asReversed())
        })
    }

}