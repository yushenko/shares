package android.yushenko.shares.ui.search

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SearchView
import android.yushenko.shares.ui.activity.MainActivity
import android.yushenko.shares.R
import android.yushenko.shares.di.MyApplication
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.idapgroup.lifecycle.ktx.observe
import kotlinx.android.synthetic.main.search_fragment.*
import javax.inject.Inject


class SearchFragment : Fragment(R.layout.search_fragment) {

    @Inject lateinit var viewModel: SearchViewModel

    private val adapterSearch = SearchAdapter {
        viewModel.addItemFollow(it)
        (activity as MainActivity).nawController.popBackStack()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (view.context.applicationContext as MyApplication).getAppComponent().inject(this)

        setupObserving()


        recyclerSearch.layoutManager = LinearLayoutManager(requireActivity())
        recyclerSearch.adapter = adapterSearch

        textViewInfo.visibility = View.INVISIBLE

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.isNotEmpty()) loadSearch(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.i("TAG", "this: $newText")
                if (newText.isNotEmpty()) {
                    Log.i("TAG", "$newText")
                    loadSearch(newText)
                }
                return true
            }
        })


        btnCancel.setOnClickListener {
            (activity as MainActivity).nawController.popBackStack()
        }

    }

    private fun visible(found: Boolean) {
        if (found) {
            textViewInfo.visibility = View.INVISIBLE
        } else {
            textViewInfo.visibility = View.VISIBLE
            textViewInfo.setText(R.string.text_not_search_found)
        }
    }

    private fun loadSearch(query: String) {
        viewModel.getSearch(query)
    }

    private fun setupObserving() {
        observe(viewModel.liveSearchEndpointData, {
            if (it.searches != null) {
                if (it.searches.isNotEmpty()) {
                    visible(true)
                    adapterSearch.setData(it.searches)
                } else {
                    adapterSearch.setData(mutableListOf())
                    visible(false)
                }
            } else {
                adapterSearch.setData(mutableListOf())
                visible(false)
            }

        })
    }
}