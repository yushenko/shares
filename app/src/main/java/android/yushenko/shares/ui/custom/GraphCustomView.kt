package android.yushenko.shares.ui.custom

import android.content.Context
import android.graphics.*
import android.graphics.Shader.TileMode.CLAMP
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.yushenko.shares.data.models.graph.Result
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.ceil
import kotlin.math.round


class GraphCustomView : View {

    private val markers: MutableList<Result> = mutableListOf()
    private var numColumns = 5
    private var numRows = 4

    private val paddingLef = 100
    private val paddingLeftCol = 50
    private val paddingBot = 50

    private val colorMiddleLine = Color.BLACK
    //color line
    private var colorLine = Color.GREEN
    private val colorGradient = Color.WHITE

    private val colorText= Color.BLACK

    private val colorTable= Color.GRAY
    private val colorLeftLine= Color.GRAY
    private val colorBottomLine= Color.GRAY

    private var pxPerUnit: Int = 0
    private var zeroY: Float = 0f

    fun setData(list: List<Result>) {
        markers.clear()
        markers.addAll(list)
        invalidate()
    }

    fun setGraphLineColor(color: Int) {
        colorLine = color
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (markers.isNotEmpty()) {
            calcPositions()
            drawGradient(canvas!!)
            drawTable(canvas)
            drawGraphLine(canvas)
            drawMiddleLine(canvas)
            drawWeeks(canvas)
            drawBottomLine(canvas)
            drawLeftLine(canvas)
            drawLeftText(canvas)
        }
    }

    private fun calcPositions() {
        val max = markers.maxByOrNull { it.vw }?.vw
        val min = markers.minByOrNull { it.vw }?.vw
        pxPerUnit = ((height - paddingBot - 3) / (max!! - min!!)).toInt()
        zeroY = (max * pxPerUnit).toFloat()

        val step = ((width - paddingLef) - 2 * 0) / (markers.size-1)
        for ((i, marker) in markers.withIndex()) {
            val x = (step * i + paddingLeft).toFloat()
            val y = (zeroY - marker.vw * pxPerUnit).toFloat()

            getTimeR(marker.t / 1000)
            marker.targetPos_x = x
            marker.targetPos_y = y
        }

    }

    private fun getTimeR(seconds: Long): Int {
        val date = Date(seconds * 1000L)
        val d = SimpleDateFormat("dd", Locale.US).format(date).toInt()
        return d
    }

    private fun drawTable(canvas: Canvas) {
        val path1 = Path()

        val paintLine = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = colorTable
            style = Paint.Style.STROKE
            strokeWidth = 0.5f
        }

        val step_x = (width - paddingLef - paddingLeftCol) / numColumns
        var point_x = 0
        for (i in 0..(width - paddingLef - paddingLeftCol)) {
            path1.moveTo(point_x.toFloat(), 0f)
            for (j in 0..height) {
                path1.lineTo(point_x.toFloat(), j.toFloat())
            }
            point_x += step_x
        }

        val step_y = (height - paddingBot) / numRows
        var point_y = 0
        for (i in 0..(height-paddingBot)) {
            path1.moveTo(0f, point_y.toFloat())
            for (j in 0..width) {
                path1.lineTo(j.toFloat(), point_y.toFloat())
            }
            point_y += step_y
        }
        canvas.drawPath(path1, paintLine)
    }

    private fun drawGradient(canvas: Canvas) {
        val colors = intArrayOf(colorLine, colorGradient)
        val gradient = LinearGradient(
            0f, paddingTop.toFloat(), 0f, (height - paddingBot).toFloat(),   colors, null, CLAMP
        )

        val gradientPaint = Paint().apply {
            style = Paint.Style.FILL
            shader = gradient
        }

        val patch = Path()
        patch.reset()
        patch.moveTo(paddingLeft.toFloat(), zeroY)

        for (marker in markers) {
            patch.lineTo(marker.targetPos_x, marker.targetPos_y)
        }

        patch.lineTo(markers.last().targetPos_x, zeroY)
        patch.moveTo(paddingLeft.toFloat(), zeroY)
        canvas.drawPath(patch, gradientPaint)
    }

    private fun drawGraphLine(canvas: Canvas) {
        val patch = Path()
        patch.reset()
        patch.moveTo(markers[0].targetPos_x, markers[0].targetPos_y)

        val paintGraphLine = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = colorLine
            style = Paint.Style.STROKE
            strokeWidth = 3f
        }

        for (marker in markers) {
            patch.lineTo(marker.targetPos_x, marker.targetPos_y)
        }

        canvas.drawPath(patch, paintGraphLine)
    }

    private fun drawMiddleLine(canvas: Canvas) {
        val path2 = Path()
        val paintMiddleLine = Paint().apply {
            color = colorMiddleLine
            style = Paint.Style.STROKE
            strokeWidth = 1f
            pathEffect = DashPathEffect(floatArrayOf(30f, 15f), 0f)
        }

        val middle_y = middle(markers)
        path2.moveTo(0f, middle_y.toFloat())
        for (i in 0..((width - paddingLef))) {
            path2.lineTo(i.toFloat(), middle_y.toFloat())
        }
        canvas.drawPath(path2, paintMiddleLine)
    }

    private fun drawBottomLine(canvas: Canvas) {
        val path = Path()
        val paintMiddleLine = Paint().apply {
            color = colorBottomLine
            style = Paint.Style.STROKE
            strokeWidth = 1f
        }

        val linePos = height - paddingBot.toFloat()
        path.moveTo(0f, linePos)
        for (i in 0..width) {
            path.lineTo(i.toFloat(), linePos)
        }
        canvas.drawPath(path, paintMiddleLine)
    }

    private fun drawLeftLine(canvas: Canvas) {
        val path = Path()
        val paintMiddleLine = Paint().apply {
            color = colorLeftLine
            style = Paint.Style.STROKE
            strokeWidth = 0.5f
        }

        for (i in 0..height - paddingBot) {
            path.lineTo(width - paddingLef.toFloat(),  i.toFloat())
        }
        canvas.drawPath(path, paintMiddleLine)
    }

    private fun drawLeftText(canvas: Canvas) {
        val path = Path()
        val paintText = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = colorText
            textSize = 35f
        }

        val max = (markers.maxByOrNull { it.vw }?.vw)?.toFloat()
        val min = (markers.minByOrNull { it.vw }?.vw)?.toFloat()

        val stepY = (height - paddingBot) / numRows
        var interY = 0
        calcInterval(max!!, min!!).forEach {
            Log.i("TAG", "val: $it")
            canvas.drawText("$it", width - paddingLef + 10f, interY + 35f, paintText)
            interY += stepY
        }
        canvas.drawPath(path, paintText)
    }

    private fun calcInterval(max: Float, min: Float): Array<Int?> {
        val step = (max - min) / numRows
        val arr = arrayOfNulls<Int>(4)
        var inter = 0f
        for (i in 0..3) {
            arr[i] = round(max - inter).toInt()
            inter += step
        }
        return arr
    }


    private fun drawWeeks(canvas: Canvas) {
        val patch = Path()
        patch.reset()
        patch.moveTo(paddingLeft.toFloat(), zeroY)

        val paintText = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = colorText
            textSize = 35f
        }

        val step_x = (width - paddingLef) / markers.size
        var point_x = 0
        var k = 0
        for (i in 0 until markers.size) {
            if (k > 3) {k = 0}
            if(k == 0 ) {
                canvas.drawText("${getTime(markers[i].t/1000)}", markers[i].targetPos_x + 8f, height-10f, paintText)
            }
            k++
            point_x += step_x
        }
        canvas.drawPath(patch, paintText)
    }

    private fun getTime(seconds: Long): String? {
        val date = Date(seconds * 1000L)
        return SimpleDateFormat("dd", Locale.US).format(date)
    }

    private fun middle(list: MutableList<Result>): Int {
        var sum = 0.0
        for (result in list) sum += result.targetPos_y
        return sum.toInt() / list.size
    }
}

