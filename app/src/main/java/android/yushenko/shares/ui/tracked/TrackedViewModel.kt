package android.yushenko.shares.ui.tracked

import android.yushenko.shares.data.models.company.QuoteEndpoint
import android.yushenko.shares.data.models.graph.DataDays
import android.yushenko.shares.data.models.news.News
import android.yushenko.shares.data.repository.SharesRepository
import android.yushenko.shares.ui.shared.BaseViewModel
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import javax.inject.Inject

class TrackedViewModel @Inject constructor(
    private val repository: SharesRepository
) : BaseViewModel() {

    val sharesLiveData = MutableLiveData<QuoteEndpoint>()
    val newsLiveData = MutableLiveData<News>()
    val dataDaysLiveData = MutableLiveData<DataDays>()

    fun loadSharesData(symbol: String) = launch {
        sharesLiveData.postValue(repository.getDataShares(symbol))
        newsLiveData.postValue(repository.getNewsCompany(symbol))
        dataDaysLiveData.postValue(repository.getDataDays(symbol))
    }

    fun unFollow(symbol: String) = launch {
        repository.deleteItem(symbol)
    }
}